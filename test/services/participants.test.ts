import assert from 'assert';
import app from '../../src/app';

describe('\'participants\' service', () => {
  it('registered the service', () => {
    const service = app.service('participants');

    assert.ok(service, 'Registered the service');
  });
});
