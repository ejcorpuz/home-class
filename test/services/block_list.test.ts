import assert from 'assert';
import app from '../../src/app';

describe('\'block_list\' service', () => {
  it('registered the service', () => {
    const service = app.service('block-list');

    assert.ok(service, 'Registered the service');
  });
});
