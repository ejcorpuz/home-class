import assert from 'assert';
import app from '../../src/app';

describe('\'deleted_messages\' service', () => {
  it('registered the service', () => {
    const service = app.service('deleted-messages');

    assert.ok(service, 'Registered the service');
  });
});
