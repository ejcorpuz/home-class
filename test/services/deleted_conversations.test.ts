import assert from 'assert';
import app from '../../src/app';

describe('\'deleted_conversations\' service', () => {
  it('registered the service', () => {
    const service = app.service('deleted-conversations');

    assert.ok(service, 'Registered the service');
  });
});
