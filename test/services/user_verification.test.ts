import assert from 'assert';
import app from '../../src/app';

describe('\'user_verification\' service', () => {
  it('registered the service', () => {
    const service = app.service('user-verification');

    assert.ok(service, 'Registered the service');
  });
});
