import assert from 'assert';
import app from '../../src/app';

describe('\'user_contact\' service', () => {
  it('registered the service', () => {
    const service = app.service('user-contact');

    assert.ok(service, 'Registered the service');
  });
});
