import assert from 'assert';
import app from '../../src/app';

describe('\'user-verification\' service', () => {
  it('registered the service', () => {
    const service = app.service('user-verification');

    assert.ok(service, 'Registered the service');
  });
});
