// Initializes the `access` service on path `/access`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Access } from './access.class';
import hooks from './access.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'access': Access & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/access', new Access(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('access');

  service.hooks(hooks);
}
