import { Application } from '../declarations';
import users from './users/users.service';
import devices from './devices/devices.service';
import userContact from './user_contact/user_contact.service';
import access from './access/access.service';
import blockList from './block_list/block_list.service';
import userVerification from './user_verification/user_verification.service';
import participants from './participants/participants.service';
import messages from './messages/messages.service';
import contacts from './contacts/contacts.service';
import attachments from './attachments/attachments.service';
import deletedMessages from './deleted_messages/deleted_messages.service';
import deletedConversations from './deleted_conversations/deleted_conversations.service';
import reports from './reports/reports.service';
import activities from './activities/activities.service';
import conversation from './conversation/conversation.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(users);
  app.configure(devices);
  app.configure(userContact);
  app.configure(access);
  app.configure(blockList);
  app.configure(userVerification);
  app.configure(participants);
  app.configure(messages);
  app.configure(contacts);
  app.configure(attachments);
  app.configure(deletedMessages);
  app.configure(deletedConversations);
  app.configure(reports);
  app.configure(activities);
  app.configure(conversation);
}
