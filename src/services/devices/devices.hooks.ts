import * as authentication from '@feathersjs/authentication';
import { HookContext } from '@feathersjs/feathers';
import Joi from '@hapi/joi';
import validate from '@feathers-plus/validate-joi';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

const schema = Joi.object().keys({
  user_id: Joi.string().uuid().trim().required(),
  device_id: Joi.string().trim().required(),
  type: Joi.string().trim().required(),
  device_token: Joi.string().trim().required(),
});

export default {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [
      validate.form(schema),
      async (context: HookContext) => {
        return context;
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
