// Initializes the `deleted_conversations` service on path `/deleted-conversations`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { DeletedConversations } from './deleted_conversations.class';
import hooks from './deleted_conversations.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'deleted-conversations': DeletedConversations & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/deleted-conversations', new DeletedConversations(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('deleted-conversations');

  service.hooks(hooks);
}
