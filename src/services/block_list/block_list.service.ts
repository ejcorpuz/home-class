// Initializes the `block_list` service on path `/block-list`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { BlockList } from './block_list.class';
import hooks from './block_list.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'block-list': BlockList & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/block-list', new BlockList(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('block-list');

  service.hooks(hooks);
}
