import * as feathersAuthentication from '@feathersjs/authentication';
import * as local from '@feathersjs/authentication-local';
import Joi from '@hapi/joi';
import validate from '@feathers-plus/validate-joi';
import { HookContext } from '@feathersjs/feathers';

// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = feathersAuthentication.hooks;
const { hashPassword, protect } = local.hooks;

const schema = Joi.object().keys({
  first_name: Joi.string().trim().min(1).max(30).required(),
  last_name: Joi.string().trim().min(1).max(30).required(),
  email: Joi.string().email().required(),
  password: Joi.string().trim().min(6).max(32).required(),
  phone: Joi.number().integer()
});

export default {
  before: {
    all: [],
    find: [authenticate('jwt')],
    get: [authenticate('jwt')],
    create: [
      validate.form(schema),
      hashPassword('password'),
      async (context: HookContext) => {
        if (await context.service.isEmailUsed(context.data.email)) {
          throw new Error('User with the same email address already exists.');
        }

        return context;
      }
    ],
    update: [hashPassword('password'), authenticate('jwt')],
    patch: [hashPassword('password'), authenticate('jwt')],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password'),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
