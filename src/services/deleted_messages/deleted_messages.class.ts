import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { DatabaseService } from '../database';

interface Data { }

interface ServiceOptions { }

export class DeletedMessages extends DatabaseService implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    super();
    this.tableName = 'deleted_messages';
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    return await this._find(this.fields(params));
  }

  async get(id: Id, params?: Params): Promise<Data> {
    return await this._get(id);
  }

  async create(data: Data, params?: Params): Promise<Data> {
    return await this._create(data);
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return await this._update(id, data);
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return await this._update(id, data);
  }

  async remove(id: Id, params?: Params): Promise<Data> {
    return await this._remove(id);
  }
}
