// Initializes the `deleted_messages` service on path `/deleted-messages`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { DeletedMessages } from './deleted_messages.class';
import hooks from './deleted_messages.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'deleted-messages': DeletedMessages & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/deleted-messages', new DeletedMessages(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('deleted-messages');

  service.hooks(hooks);
}
