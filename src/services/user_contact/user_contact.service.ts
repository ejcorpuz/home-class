// Initializes the `user_contact` service on path `/user-contact`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { UserContact } from './user_contact.class';
import hooks from './user_contact.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'user-contact': UserContact & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/user-contact', new UserContact(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('user-contact');

  service.hooks(hooks);
}
