import { r, Connection } from 'rethinkdb-ts';
import { Paginated, Params } from '@feathersjs/feathers';
import { filterQuery } from '@feathersjs/adapter-commons';

interface Fields {
  [key: string]: any;
}


export class DatabaseService {
  tableName: string;

  constructor() {
    this.tableName = '';
  }

  fields(params?: Params) {
    const { filters, query } = filterQuery(params?.query);

    return Object.keys(query).reduce((object: any, key: any) => {
      if (!Object.keys(filters).includes(key)) {
        object[key] = query[key];
      }
      return object;
    }, {});
  }

  async connection() {
    const conn = await r.connect({ host: '127.0.0.1', port: 28015, db: 'test' });
    const result = async (dbc: Connection) => {
      try {
        await r.tableCreate(this.tableName).run(dbc);
        return dbc;
      }
      catch (error) {
        switch (error.name) {
          case 'ReqlOpFailedError':
            return dbc;
        }
        throw error;
      }
    };
    return result(conn);
  }

  async _find(fields: Fields): Promise<any[] | Paginated<any>> {
    return this.connection().then(async (conn) => {
      return await r.table(this.tableName).filter(fields).run(conn);
    });
  }

  async _count(fields: Fields): Promise<Number> {
    return this.connection().then(async (conn) => {
      return await r.table(this.tableName).filter(fields).count().run(conn);
    });
  }

  async _get(id: string | number): Promise<any> {
    return this.connection().then(async (conn) => {
      return await r.table(this.tableName).get(id).run(conn);
    });
  }

  async _create(fields: Fields): Promise<any> {
    return this.connection().then(async (conn) => {
      return await r.table(this.tableName).insert(fields).run(conn).then(async (res) => {
        return await r.table(this.tableName).get(res.generated_keys?.filter(x => typeof x !== undefined).shift()).run(conn);
      });
    });
  }

  async _update(id: string | number | null, fields: Fields): Promise<any> {
    return this.connection().then(async (conn) => {
      return await r.table(this.tableName).get(id).update(fields).run(conn).then(async (res) => {
        return await r.table(this.tableName).get(id).run(conn);
      });
    });
  }

  async _remove(id: string | number): Promise<any> {
    return this.connection().then(async (conn) => {
      return await r.table(this.tableName).get(id).delete().run(conn);
    });
  }
};