// Initializes the `participants` service on path `/participants`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Participants } from './participants.class';
import hooks from './participants.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'participants': Participants & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/participants', new Participants(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('participants');

  service.hooks(hooks);
}
