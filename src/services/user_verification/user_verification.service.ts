// Initializes the `user_verification` service on path `/user-verification`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { UserVerification } from './user_verification.class';
import hooks from './user_verification.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'user-verification': UserVerification & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/user-verification', new UserVerification(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('user-verification');

  service.hooks(hooks);
}
